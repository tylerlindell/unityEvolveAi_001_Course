﻿using System.Collections;
using System.Collections.Generic;
using SharpNeat.Phenomes;
using UnityEngine;

public class LocomotionController : UnitController
{
    private Rigidbody rBody;
    private bool IsRunning;
    private IBlackBox box;

    public float fitness;
    public GameObject target;
    public float force = 5f;

    // Use this for initialization
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    // FixedUpdate called every fixed framerate frame, if the MonoBehaviour is enabled.
    //FixedUpdate should be used instead of Update when dealing with Rigidbody.
    void FixedUpdate()
    {
        //verify we are running
        if (IsRunning)
        {
            // initialize input and output signal arrays
            ISignalArray inputArr = box.InputSignalArray;
            ISignalArray outputArr = box.OutputSignalArray;

            //use vertical position as input
            //this is so the Neural Net (NN) knows where the cube is in vertical space
            //it can use this value to judge when a thrust should be triggered
            inputArr[0] = transform.position.x;
            inputArr[1] = transform.position.y;
            inputArr[2] = transform.position.z;

            //apply force as long as the cube is within a certain distance from a given value
            //the given value is decided on by the Neural Net (NN) based on input values
            if (transform.position.y < (float)outputArr[0])
            {
                //inform the NN that a force is being applied to the cube
                inputArr[3] = 1;

                //allow the NN to decide on how much force is going to be applied with the value from `outputArr[1]`
                rBody.AddForce(Vector3.up * (float)outputArr[1] * force, ForceMode.Impulse);
                
                //allow the NN to decide on which direction angular velocity is going to be 
                //applied with the values from `outputArr[2]` through `outputArr[4]`
                rBody.angularVelocity = new Vector3((float)outputArr[2] * force, (float)outputArr[3] * force, (float)outputArr[4] * force);

            }
            else
            {
                //inform the NN that zero force is being applied to the cube
                inputArr[3] = 0;
            }

            //send the fitness of each cube to the NN so it always has a gauge of how its doing at all times
            //not just at the end of a training session
            inputArr[4] = fitness;
            box.Activate();

            //determin our heading
            Vector3 heading = target.transform.position - transform.position;

            //get our distace from our target
            float distance = heading.magnitude;


            //send a fraction with distance in the denominator
            //this is so we have a value that increases while our distance gets smaller
            //(eg. if distance is 100, our fraction is 1/100)
            //(eg. if distance is 1, our fraction is 1/1 or just a value of 1)

            if(distance > 0){//cannot divide by zero and cannot be closer to something than 0
                AddFitness(Mathf.Abs(1 / distance));
            }
        }
    }

    public override void Activate(IBlackBox box)
    {
        this.box = box;
        this.IsRunning = true;
    }

    public override float GetFitness()
    {
        var fit = fitness;//cache the fitness value
        fitness = 0;//reset fitness value each time we start a new training cycle

        if (fit < 0)
            fit = 0;

        return fit;

    }

    public override void Stop()
    {
        this.IsRunning = false;
    }

    void AddFitness(float fit)
    {
        fitness += fit;
    }
}
